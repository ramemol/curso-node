// Importamos el paquete express
import express from 'express'


import * as productos from './productos'


// Instanciamos una app de express
const app = express()
app.use(express.json());


// Definimos una ruta y su handler correspondiente
app.get('/', function (request, response) {
    response.send('¡Bienvenidos a Express!')
})


// Ponemos a escuchar nuestra app de express
app.listen(3000, function () {
    console.info('Servidor escuchando en http://localhost:3000')
});

app.get('/productos',function(request, response) {
    response.send( productos.getStock());
});


app.post('/productos', function(request, response) {
    const body = request.body
    response.send(productos.storeProductos(body));
});


app.delete('/productos/:id', function (request, response){
    const ind= parseInt(request.params.id);
    response.send(productos.deleteProductos(ind));

});

app.put('/productos/:id', function (request, response){
    const body = request.body;
    const ind = parseInt(request.params.id);
    response.send(productos.putProductos(body,ind));
});