// funcion con nombre o nombrada
function suma(x: number , y: number):number {
    return x + y
}
// funcion flecha
const sumaFlecha = (x: number , y: number):number => {
    return x + y
}
// console.log(suma(1,2) );

// funcion con parametros opcionales
const funcionOpcionalSuma = (x:number, y?:number):number =>{
    if(!y) return x 
    return x + y
}
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));

// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x:number, y:number = 0):number =>{
    return x + y
}
console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1,2));



//funcion flecha
const divisionFlecha = (x: number , y: number):number|undefined => {
    if (y !== 0) return x / y
    console.error('No se puede dividir por 0')
}

//funcion flecha
const multiplicacionFlecha = (x: number, y: number):number =>{
    return x * y;
}

console.log(divisionFlecha(1,2));
console.log(divisionFlecha(1,0));
console.log(multiplicacionFlecha(2,2));

